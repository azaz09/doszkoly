<?php

use yii\helpers\Html;
use yii\grid\GridView;
/* @var $this yii\web\View */
/* @var $searchModel common\models\search\UsersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Użytkownicy';
?>
    <div class="my-5 py-5 row">
        <div class="col-lg-12 col-md-12 col-xs-12">
            <h1 class="opt-header"><?= Html::encode($this->title) ?></h1>
            <?php // echo $this->render('_search', ['model' => $searchModel]);  ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-xs-12 btn-create">
            <?= Html::a('Stwórz użytkownika', ['create'], ['class' => 'btn btn-lg btn-success']) ?>
        </div>
    </div>
    <div class='panel panel-default mx-5 my-5 px-5 py-5 text-center'>
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                //'id',
                'username',
                //'auth_key',
                //'password_hash',
                // 'password_reset_token',
                //'email:email',
                //'status',
                'option',
                //'created_at',
                //'updated_at',
                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]);
        ?>
    </div>
