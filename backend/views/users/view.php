<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\Users;

/* @var $this yii\web\View */
/* @var $model common\models\Users */
$this->title = $model->username;
?>
<div class="panel panel-default">
    <div class='panel-heading'>
        <div class="row my-5 text-center">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <h1 class="text-warning"><?= Html::encode($this->title) ?></h1>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12"> 
                <p>
                    <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-lg btn-primary']) ?>
                    <?=
                    Html::a('Delete', ['delete', 'id' => $model->id], [
                        'class' => 'btn btn-lg btn-danger',
                        'data' => [
                            'confirm' => 'Are you sure you want to delete this item?',
                            'method' => 'post',
                        ],
                    ])
                    ?>
                </p>
            </div>

        </div>
    </div>
    <div class="panel-body text-center row my-5">
        <div class="col-lg-12 col-md-12 col-sm-12 py-5">


            <?=
            DetailView::widget([
                'model' => $model,
                'attributes' => [
                    //'id',
                    'username',
                    //'auth_key',
                    // 'password_hash',
                    //'password_reset_token',
                    'email:email',
                    //'status',
                    'option',
                //'created_at',
                //'updated_at',
                ],
            ])
            ?>


        </div>
    </div>
</div>