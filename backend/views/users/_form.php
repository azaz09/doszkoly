<?php

use yii\helpers\Html;
use yii\helpers\BaseHtml;
use yii\widgets\ActiveForm;
use common\models\Users;

/* @var $this yii\web\View */
/* @var $model common\models\Users */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="users-form bg-img-circle">

    <?php $form = ActiveForm::begin(); ?>
    <div class='panel panel-default my-5'>
        <div class="panel-heading">
        <div class='row text-center'>
            <div class='col-lg-12 col-md-12 col-sm-12'>
                <h3 class="text-primary my-3"><?= $this->title = 'Tworzenie użytkownika'; ?></h3>
            </div>
        </div>
        </div>
            <div class='panel-body'>
                <div class='row'>
                    <div class='col-lg-12 col-md-12 sm-12 mb-5'>
                        <h6>Info: </h6> -Przy edycji pokazane jest zahashowane hasło
                        <hr/>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-6"><?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?></div>
                    <div class="col-lg-4 col-md-4 col-sm-6"><?= $form->field($model, 'password_hash')->passwordInput(['maxlength' => true]) ?></div>
                    <div class="col-lg-4 col-md-4 col-sm-6"><?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?></div>
                </div>
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-6 col-lg-offset-4 col-md-offset-4 col-sm-offset-3"><?=
                                $form->field($model, 'option')->checkbox(array(
                                    'label' => '',
                                    'labelOptions' => array('style' => 'padding: 6px; margin: 10px;')))
                                ->label('Pozwolenie na edycje');
                        ?></div>
                </div>
            </div>
            <div class="row text-center">
                <div class="col-lg-12 col-md-12 col-sm-12 my-5">
                        <?= Html::submitButton('Zapisz', ['class' => 'btn btn-success btn-lg']) ?>
                </div>
            </div>



            <?php ActiveForm::end();
            ?>

        </div>
