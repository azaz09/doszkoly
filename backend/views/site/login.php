<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\web\View;
use \common\models\LoginForm;

$this->title = 'Login';
?>
<div class=" site-login well well-lg">
    <div class="row text-dark">
        <div class="col-lg-12 col-md-12">
            <h1 class="opt-header"><?= Html::encode($this->title) ?></h1>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-10 mx-5">
            <p>Wypełnij poprawnie pola, aby się zalogować:</p>
            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

            <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>

            <?= $form->field($model, 'password')->passwordInput() ?>

            <?= $form->field($model, 'rememberMe')->checkbox() ?>

            <div class="form-group">
                <?= Html::submitButton('Login', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
        <div class="ml-5 col-lg-6 col-md-6 col-sm-10 py-5 px-5 my-5 text-white text-justify well well-lg bg-dark ">
            <p>
                Panel administracyjny przeznaczony jest dla użytku szkoły.<br/>
                Dostęp do panelu posiadają jedynie administratorzy.<br/><br/>
                W kwestii współpracy z twórcą proszę pisać na e-mail: nanquery@gmail.com
                
            </p>
        </div>
    </div>
</div>
