<?php
/* @var $this yii\web\View */

use common\models\Users;

$this->title = 'Aplikacja Yii';
?>

<!--in container-FLUID-->

<div class="row mx-5 my-5 text-center">
    <div class='col-lg-12 col-md-12 col-sm-12'> 
        
        <h1 class='opt-header'><i class="fab fa-fort-awesome-alt fa-7x mb-2"></i><br/>
            Witaj <?= Yii::$app->user->identity->username ?> !
        </h1>
    </div>
</div>
<div class="row my-5 text-center">
    <div class="col-lg-12 col-md-12 col-sm-12">
         <p class="lead text-white">Tutaj znajdziesz prosty CMS stworzony na potrzeby strony i więcej informacji na temat użytego przy projekcie framework'a</p>

        <p><a class="btn btn-lg btn-primary my-5" href="http://www.yiiframework.com">Start with Yii</a></p>
    </div>
</div>
       
   
