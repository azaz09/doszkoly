<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Article */

$this->title = 'Edytuj artykuł';

?>
<div class="article-update">

    

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
