<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\Article;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\ArticleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$model = new Article();
$this->title = 'Artykuły';
?>
<div id="art-page" class="bg-hex" style="text-align: justify; text-justify: inter-word;">
    <div class="my-5 py-5 row">
        <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
            <h1 class="opt3-header"><?= Html::encode($this->title) ?></h1>
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
        </div>
    </div>

    <div class="row text-center my-5">
        <div class='col-lg-12 col-md-12 col-sm-12'>
            <p>
                <?= Html::a('Stwórz Artykuł', ['create'], ['class' => 'btn btn-success btn-lg']) ?> 
            </p>
        </div>
    </div>

<!--EE.08-->
    <div id='ee08' class='row mt-5'>

        <div class='col-lg-12 col-md-12 col-sm-12 text-center my-5'>

            <h4>EE.08  <span class="badge badge-info badge-pill"><?php $model->howMany('EE.08') ?></span></h4>
        </div>
    </div>
    <div class='row mt-0 pt-0 text-center'>
        <?php $model->listArt('EE.08'); ?>
    </div>
    <?php $model->showArt('EE.08'); ?>
    
<!--EE.09-->
    <div id='ee09' class='row mt-5'>

        <div class='col-lg-12 col-md-12 col-sm-12 text-center my-5'>

            <h4>EE.09  <span class="badge badge-success badge-pill"><?php $model->howMany('EE.09') ?></span></h4>
        </div>
    </div>
    <div class='row mt-0 pt-0 text-center'>
        <?php $model->listArt('EE.09'); ?>
    </div>
    <?php $model->showArt('EE.09'); ?>

<!--Inne-->
     <div id="inne" class='row mt-5'>

        <div class='col-lg-12 col-md-12 col-sm-12 text-center my-5'>

            <h4>Inne  <span class="badge badge-danger badge-pill"><?php $model->howMany('inne') ?></span></h4>
        </div>
    </div>
    <div class='row mt-0 pt-0 text-center'>
        <?php $model->listArt('inne'); ?>
    </div>
    <?php $model->showArt('inne'); ?>
</div>