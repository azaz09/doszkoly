<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model common\models\Article */
/* @var $form yii\widgets\ActiveForm */
$this->title = 'Stwórz artykuł';
?>

<div class="article-form my-5">

    <?php $form = ActiveForm::begin(); ?>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="text-primary text-center"><?= Html::encode($this->title) ?></h3><br/>
        </div>
        <div class="panel-body">
            <div class='row mx-5'>
                <div class='col-lg-12 col-md-12 col-sm-12'>
                    <?= $form->field($model, 'imageUrl')->textInput(['maxlength' => true]) ?>
                </div>
            </div>
            <div class="mx-auto row">
                <div class='mx-auto col-lg-5 col-md-5 col-sm-5'>
                    <?= $form->field($model, 'title')->textInput(['rows' => 6]) ?>
                </div>
                <div class='mx-auto col-lg-5 col-md-5 col-sm-5'>
                    <?= $form->field($model, 'topic')->textInput(['rows' => 6]) ?>
                </div>
            </div>
            <div class="row mx-5">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <?= $form->field($model, 'content')->textarea(['rows' => 6], ['minlength' => 300]) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <?= $form->field($model, 'footnote')->textarea(['rows' => 2]) ?>
                </div>
            </div>
            <pre class="text-dark">Info: dla lepszego efektu notatki używaj dokładnie tagu: "li class='list-group-item'" i class dostępnych w bootstrap'ie</pre>
        </div>
        <div class="row text-center">
            <div class="mx-auto col-lg-3 col-md-3 col-sm-3">
                <?=
                $form->field($model, 'category')->dropdownList([
                    'EE.08' => 'EE.08',
                    'EE.09' => 'EE.09',
                    'inne' => 'inne',
                        ], ['prompt' => 'Wybierz']
                );
                ?>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 mx-auto">
                <?=
                $form->field($model, 'updated_at')->widget(DatePicker::className(), [
                    'options' => ['placeholder' => 'Data edycji / powstania'],
                    'type' => DatePicker::TYPE_INPUT,
                    'class' => '',
                    'value' => 'Podaj datę dodania lub edycji',
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'dd | MM | yyyy'
                    ]
                ]);
                ?> 
            </div>
        </div>

    </div>

    <div class="row text-center">
        <div class='col-lg-12 col-md-12 col-sm-12 mb-5'>
            <?= Html::submitButton('Save', ['class' => 'btn btn-success btn-lg']) ?>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>

