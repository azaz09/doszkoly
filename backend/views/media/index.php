<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\Media;

$model = new Media;
/* @var $this yii\web\View */
/* @var $searchModel common\models\search\MediaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Media';
?>
<div class="media-index text-center">
    <div class="row my-5 py-5">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <h1 class="opt4-header"><?= Html::encode($this->title) ?></h1>
        </div>
    </div>
    <div class="row my-5 py-5">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <p>
<?= Html::a('Dodaj media', ['create'], ['class' => 'btn btn-success btn-lg']) ?>
            </p>
        </div>
    </div>
    <div class="row px-5 my-5">
        <div class="img col-lg-12 col-md-12 col-sm-12 px-5"><hr/><h3 class="text-success">Zdjęcia</h3><hr/></div>
    </div>
    
    
    <div class = 'd-flex flex-row justify-content-center flex-wrap my-4'>
<?= $model->showMedia('image','EE.08'); ?>
<?= $model->showMedia('image','EE.09'); ?>
<?= $model->showMedia('image','inne'); ?>
<?= $model->showMedia('image','sometimes'); ?>
<?= $model->showMedia('image','wszystkie'); ?>
    </div>
    <div class="row px-5 my-5">
        <div class="img col-lg-12 col-md-12 col-sm-12 px-5"><hr/><h3 class="text-danger">Filmy</h3><hr/></div>
    </div>
     <div class = 'd-flex flex-row justify-content-center flex-wrap my-4'>
<?= $model->showMedia('video','EE.08'); ?>
<?= $model->showMedia('video','EE.09'); ?>
<?= $model->showMedia('video','inne'); ?>
<?= $model->showMedia('video','wszystkie'); ?>
       
    </div>

</div>
