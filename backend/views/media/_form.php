<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Media */
/* @var $form yii\widgets\ActiveForm */
$this->title = 'Dodaj zdjęcie lub wideo';
?>
<div class="panel panel-default text-center">
    <div class="panel-heading">
        <div class="row"> 
            <div class="col-lg-12 col-md-12 col-sm-12">
                <h3 class="opt2-header"><?= Html::encode($this->title) ?></h3>
                <pre class="my-4">Info: Dodając film z youtube zmień: " https://www.youtube.com/<span class="text-danger">watch?v=</span>IvVfyKBnUKI" na
                    https://www.youtube.com/<span class="text-success">embed/</span>IvVfyKBnUKI
            </div>
        </div>
    </div>
    <?php $form = ActiveForm::begin(); ?>
    <div class="panel-body">
        <div class="row my-5">
            <div class="col-lg-10 col-md-10 col-sm-10 mx-auto mx-5 px-5">
                <?= $form->field($model, 'url')->textInput(); ?>
            </div>
        </div>
        <div class="row my-5">
            <div class="text-warning mx-auto col-lg-3 col-md-3 col-sm-5">
                <?=
                    $form->field($model, 'type')->dropdownList([
                        'image' => 'image',
                        'video' => 'video',
                            ], ['prompt' => 'Wybierz']
                    );
                    ?>
            </div>
        </div>
        <div class="row my-5">
            <div class="mx-auto col-lg-4 col-md-4 col-sm-5">
                <?=
                    $form->field($model, 'page')->dropdownList([
                        'EE.08' => 'EE.08',
                        'EE.09' => 'EE.09',
                        'sometimes' => 'Dzień z życia',
                        'inne' => 'inne',
                            ], ['prompt' => 'Wybierz']
                    );
                    ?>
            </div>
            <div class="mx-auto col-lg-5 col-md-5 col-sm-7">
                <?= $form->field($model, 'content')->textInput(); ?>
            </div>
        </div>
    

        <div class="row my-5">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <?= Html::submitButton('Zapisz', ['class' => 'btn btn-success btn-lg']) ?>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
