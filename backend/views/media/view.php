<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Media */

$this->title = $model->id;
?>
<div class="panel panel-default">
    <div class="panel-heading">
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12">
        <h3 class="text-primary"><?= Html::encode($this->title) ?></h3>
    </div>
</div>
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12">
        <p>
            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?=
            Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </p>
    </div>
</div>
    </div>
    <div class="panel-body">
<div class="row">
    <div class="col-lg-10 col-md-10 col-sm-10">
<?=
DetailView::widget([
    'model' => $model,
    'attributes' => [
        'id',
        'imageUrl',
        'videoUrl',
        'type:ntext',
        'content:ntext',
        'created_at',
        'updated_at',
    ],
])
?>
    </div>
</div>
</div>
</div>