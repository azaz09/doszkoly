<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Media */

$this->title = 'Edytowanie Mediów: ' . $model->content;
?>
<div class="media-update">
    <div class="panel panel-default">
        <div class="panel-heading py-5">
    <div class="row text-center">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <h1 class="text-primary"><?= Html::encode($this->title) ?></h1>
        </div>
    </div>
        </div>
    </div>

    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>

</div>
