<?php
 
namespace console\controllers;
 
use common\models\User;
use yii\console\Controller;
use yii\helpers\Console;
 
class UserController extends Controller {
 
    /**
     *
     * @param string $name     admin name.
     * @param string $password admin password.
     * @param string $email    The email address.
     *
     * @return integer
     */
    public function actionCreate($name, $password, $email) {
 
        $user = new User();
       /* $user->setName($name)->setEmail($email)->setPassword($password);
        $user->save(false); */
        $user->username=$name;
        $user->password=$password;
        $user->email=$email;
        $user->save();
        echo $this->ansiFormat('Konto stworzone.' . PHP_EOL, Console::FG_GREEN);
 
        return 0;
    }
 
    /**
     * Changes admin password.
     *
     * @param string $password
     *
     * @return int
     */
    public function actionChangePassword($username, $password) {
        $user = Admin::findOne(['username' => $username]);
 
        if (null === $user) {
            echo $this->ansiFormat('User not found' . PHP_EOL, Console::FG_RED);
            echo $this->ansiFormat(
                    'To create admin user use command php yii admin/create password.' . PHP_EOL, Console::FG_CYAN
            );
 
            return 1;
        }
       
        $user->setPassword($password);
        $user->save(false);
 
        echo $this->ansiFormat('Password changed' . PHP_EOL, Console::FG_GREEN);
 
        return 0;
    }
 
}