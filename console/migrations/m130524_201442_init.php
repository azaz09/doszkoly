<?php

use yii\db\Migration;

class m130524_201442_init extends Migration {

    public function up() {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('users', [
             'id' => $this->primaryKey(),
            'username' => $this->string()->notNull()->unique(),
            'auth_key' => $this->string(32),
            'password_hash' => $this->string()->notNull(),
            'password_reset_token' => $this->string()->unique(),
            'email' => $this->string()->notNull()->unique(),
            'status' => $this->boolean()->notNull()->defaultValue(10),
            'option' => $this->boolean(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
                ], $tableOptions);

        $this->createTable('article', [
            'id' => $this->primaryKey(),
            'imageUrl' => $this->string()->notNull(),
            'title' => $this->text()->notNull(),
            'topic' => $this->text(),
            'category' => $this->string()->notNull(),
            'content' => $this->text()->notNull(),
            'footnote' => $this->text(),
            'created_at' => $this->integer(),
            'updated_at' => $this->text(),
                ], $tableOptions);


        $this->createTable('media', [
            'id' => $this->primaryKey(),
            'url' => $this->string()->notNull(),
            'type' => $this->text()->notNull(),
            'page' => $this->text()->notNull(),
            'content' => $this->text(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp(),
                ], $tableOptions);
    }

    public function down() {
        $this->dropTable('users');
        $this->dropTable('article');
        $this->dropTable('media');
    }

}
