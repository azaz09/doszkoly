<?php

namespace common\models;

use Yii;
use yii\helpers\Html;

/**
 * This is the model class for table "article".
 *
 * @property int $id
 * @property string $imageUrl
 * @property string $title
 * @property string $topic
 * @property string $category
 * @property string $content
 * @property string $footnote
 * @property int $created_at
 * @property int $updated_at
 */
class Article extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'article';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['imageUrl', 'title', 'category', 'content', 'updated_at'], 'required'],
            [['title', 'topic', 'content', 'footnote'], 'string'],
           
            [['category'], 'string', 'max' => 255],
            [['title'], 'string', 'max' => 21],
            
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'imageUrl' => 'Adres zdjęcia',
            'title' => 'Tytuł',
            'topic' => 'podtytuł',
            'category' => 'Kategoria',
            'content' => 'Treść artykułu',
            'footnote' => 'Krótka notka',
            'created_at' => 'Created At',
            'updated_at' => 'Data dodania: ',
        ];
    }
    
    static function getContent() {
      return Article::find()->all();
    }

    public function showArt($nr) {
        $category = $nr;
        $article = Article::find()->where(['category' => $category])->all();
        foreach ($article as $art) {
            
            $img = $art['imageUrl'];
            $title = $art['title'];
            $topic = $art['topic'];
            $category = $art['category'];
            $content = $art['content'];
            $footnote = $art['footnote'];

            if (isset($_SESSION['edit'])) {
                $update = Html::a('Edytuj Artykuł', ['update', 'id' => $art['id']], ['class' => 'my-3 btn btn-info']);
                $delete = Html::a('Usuń', ['delete', 'id' => $art['id']], ['class' => 'my-3 btn btn-danger',
                            'data' => [
                                'confirm' => 'Are you sure you want to delete this item?',
                                'method' => 'post',
                            ],
                ]);
                $topic = $art['topic'] . "<br/><span style='margin: 1%;'>" . $update . "</span>" . "<span style='margin: 1%;'>" . $delete . "</span>";
            }
            
            if ($art['category'] === 'EE.08') {
                $color = 'info';
            }
            if ($art['category'] === 'EE.09') {
                $color = 'success';
            }
            if ($art['category'] === 'inne') {
                $color = 'danger';
            }

            $show = "
                
                 <article class='mx-auto my-5 py-5 row' id='".$title."'>
                       <div class='mx-auto card bg-light' style='width: 52rem; min-width: 24rem;'>
                <img src='".$img."' alt='Nie załadowano zdjęcia, spróbuj ponownie później' class='max-card-img card-img-top'>
        <div class=' card-header text-center py-3'>
            <h5><ins>".$title."</ins></h5>
                
        </div>
        <div class='card-body'>
            <h4 class='mt-3 card-title py-2'>".$topic."</h4>
                <hr class='mb-4' />
            <p class='card-text mb-3'>
                ".$content."
            </p>
        </div>
        <ul class='list-group text-light my-2'>
            ".$footnote."
        </ul>
        <div class='card-body mt-5 text-right text-".$color."'>
            <h5>Zakres: ".$category."</h5>
        </div>
        
        <div class='card-footer text-muted text-center py-3'>
            ".$art['updated_at']."
        </div>
            </div>
                    </article>";
            echo $show;
        }
    }

    public function howMany($nr) {
        $category = $nr;
        $id = 0;
        $article = Article::find()->where(['category' => $category])->all();
        foreach ($article as $art) {

            $id++;
        }
        echo $id;
    }

    public function listArt($nr) {
        $category = $nr;
        $article = Article::find()->where(['category' => $category])->all();
        $color = 'info';
        foreach ($article as $art) {

            if ($art['category'] === 'EE.09') {
                $color = 'success';
            }
            if ($art['category'] === 'inne') {
                $color = 'danger';
            }

            $list = "<a class='btn btn-outline-".$color." mx-auto py-2 my-1' data-id='". $art['title']."'>" . $art['title'] . "</a>";
            echo $list;
        }
    }

}
