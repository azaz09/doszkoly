<?php

namespace common\models;

use Yii;
use yii\helpers\Html;

/**
 * This is the model class for table "media".
 *
 * @property int $id
 * @property string $url
 * @property string $type
 * @property string $page
 * @property string $content
 * @property string $created_at
 * @property string $updated_at
 */
class Media extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'media';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['url', 'type', 'page'], 'required'],
            [['url', 'type', 'page', 'content'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    public function showMedia($type, $page) {
        $media = Media::find()->where(['type' => $type])->andWhere(['page' => $page])->all();
        $add = '';
        foreach ($media as $one) {
            if (isset($_SESSION['edit'])) {
                $update = Html::a('Edytuj', ['update', 'id' => $one['id']], ['class' => 'btn btn-info my-2 px-5']);
                $delete = Html::a('Usuń', ['delete', 'id' => $one['id']], ['class' => 'btn btn-danger my-2 px-5',
                            'data' => [
                                'confirm' => 'Are you sure you want to delete this item?',
                                'method' => 'post',
                            ],
                ]);

                $add = "<span class='mr-5'>" . $update . "</span><span class=''>" . $delete . "</span>";
            }
            if ($type === 'image') {
                $show = "
                            <div class =' py-5 flex-lg-column flex-md-column flex-sm-column mx-auto'>                               
                                <div class='box'>
                                    <a href='" . $one['url'] . "' data-lightbox='roadtrip' data-title='" . $one['content'] . "'>
                                        <img class='rounded-bottom window-size' src='" . $one['url'] . "' />
                                      </a>
                                    <span class='box-capt'>" . $one['content'] . "</span>
                                </div>
                                "
                        . $add .
                        "</div>
                        ";
            }
            if ($type === 'video') {
                $show = "
                            <div class='p-4 mx-auto flex-lg-column flex-md-column flex-sm-column'>
                            <div class='box-grey' >
                                <iframe style='height: 250px !important;' class='elementary' src='" . $one['url'] . "' frameborder='0' allow='autoplay; encrypted-media' allowfullscreen></iframe>
                            
                            </div>
                            <div class='text-center'>" . $add . "</div>
                            </div>";
            }

            echo $show;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'url' => 'Adres zdjęcia lub wideo',
            'type' => 'Typ wrzucanych mediów',
            'page' => 'Strona',
            'content' => 'W tytule',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

}
