<?php

use frontend\assets\AppAsset;
use common\widgets\Alert;
use common\models\Media;
use common\models\Article;

$article = new Article();
$media = new Media();
AppAsset::register($this);
?>
<hr/>
<h1 class='py-3 my-5 text-center text-primary'>Kwalifikacja - EE.08</h1>
<hr/>
<div class="panel panel-default py-5 my-5">
<h2 class='py-2 mb-1 pt-5 text-center text-debian lobster my-auto'>Wideo</h2>
      <div class='d-flex flex-row flex-wrap my-4 mx-auto'>
<?php $media->showMedia('video', 'EE.08'); ?>
      </div>
<h3 class='py-2 mb-1 text-center text-info lobster my-auto'>Zdjęcia</h3>
  <div class='d-flex flex-row flex-wrap my-4 mx-auto'>
<?php $media->showMedia('image', 'EE.08'); ?>
  </div>
</div>
<aside class='row my-5 py-5'> </aside>

<hr/>
<h1 class='py-3 my-5 text-center text-success'>Kwalifikacja - EE.09</h1>
<hr/>
<div class="panel panel-default py-5 my-5">
<h2 class='py-2 mb-1 pt-5 text-center text-debian lobster my-auto'>Wideo</h2>
  <div class='d-flex flex-row flex-wrap my-4 mx-auto'>
<?php $media->showMedia('video', 'EE.09'); ?>
  </div>
<h2 class='py-2 mb-1 pt-5 text-center text-info lobster my-auto'>Zdjęcia</h2>
  <div class="d-flex flex-row flex-wrap my-4 mx-auto">
<?php $media->showMedia('image', 'EE.09'); ?>
      </div>
</div>
<aside class='row my-5 py-5'> </aside>


