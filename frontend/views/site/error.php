<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
?>
<div class="site-error text-center">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="alert alert-danger">
        <?= nl2br(Html::encode($message)) ?>
    </div>

    <p class="py-5 mt-5">
       Nie znaleźliśmy strony, której szukasz na naszym serwerze. :]<br/>
       Przepraszamy
    </p>
    <p class="py-5 bg-info mb-5">
        Jeśli uważasz, że coś nie działa prawidłowo, proszę skontaktuj się z nami pod adres: nanquery@gmail.com
    </p>

</div>
