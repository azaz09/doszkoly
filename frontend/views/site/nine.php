<?php

/* @var $this yii\web\View */
use frontend\assets\AppAsset;
use common\widgets\Alert;
use common\models\Media;
use common\models\Article;
$article= new Article();
$media = new Media();
AppAsset::register($this);
?>
<div class="site-index">
           <div class="panel panel-default py-5 my-5">
    <h1 class="text-debian text-center mt-3 py-3 my-auto lobster">Wideo</h1>
    <hr/>
        <div class = 'd-flex flex-row flex-wrap my-4'>
<?= $media->showMedia('video','EE.09'); ?>
<?= $media->showMedia('video','wszystkie'); ?>
    </div>
           </div>
      <aside class='row my-5 py-5'> </aside>
    <hr class="mt-5"/>
    <h1 class="text-info text-center mt-3 py-3 lobster">Artykuły</h1>
    <hr/>
    <div class='row mx-auto mt-5'>

        <div class='col-lg-12 col-md-12 col-sm-12 text-center my-5'>

            <h4>EE.09  <span class="badge badge-success badge-pill"><?php $article->howMany('EE.09') ?></span></h4>
        </div>
    </div>
    <div class="row mt-0 text-center px-5 mx-5">
        
            <?php $article->listArt('EE.09'); ?>
      
    </div>
    <?= $article->showArt('EE.09') ?>