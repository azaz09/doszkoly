<?php

use yii\helpers\Html;
use frontend\assets\AppAsset;
?>
 <aside class='row my-5 py-5'> </aside>
<div class="panel panel-default">
    <div class="row text-center my-5">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <h1 class="text-danger  lobster">Informacje:</h1>
        </div>
    </div>
    <div class="row panel-body">
        <div class="col-lg-5 col-md-5 col-sm-6 text-justify mx-auto my-auto text-muted py-5 px-5">
            Strona udostępnia filmy różnych twórców youtube.<br/>
            Właścicielami filmów są kanały youtube, których intro widać przy załadowaniu.<br/><br/>
            Na udostępnianie filmów kanału Pasja informatyki, prowadzonego przez Państwo Mirosława Zelenta i Damiana Stelmacha posiadamy udzieloną przez nich zgodę.<br/>
        </div>
        <div class="col-lg-5 col-md-5 col-sm-6 py-5 px-5 text-center mx-auto my-auto ">
            <a href="https://www.youtube.com/user/MiroslawZelent">
                 <?= Html::img('@web/img/pi.jpg', ['alt' => 'PasjaInformatykiYT', 'class' => 'rounded-circle']); ?>
                <p class="my-auto text-info">Pasja Informatyki</p>
            </a>
        </div>
    </div>
</div>
 <aside class="my-5 py-5 text-center px-5"><hr></aside>
<div class="panel panel-default px-5 py-5" style="opacity: 0.75 !important;">
    <div class="row my-5 py-5 text-center mx-5 panel-heading">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <p style="font-size: 20px;">
                Tytuł technika informatyka uzyskuje absolwent technikum (na poziomie ISCED3) oraz absolwent szkoły policealnej (na poziomie ISCED4), o kierunku technik informatyk<br/> po zdaniu zewnętrznego egzaminu zawodowego z kwalifikacji EE.08 i EE.09.
            </p>
        </div>
    </div>
    <div class="row panel-body">

        <div class="col-lg-7 col-md-7 col-sm-12 my-5 py-5 my-auto mx-auto text-justify">
            <p>Technik informatyk po zdaniu państwowych egzaminów potwierdzających jego kwalifikacje zawodowe otrzymuje dyplom technika oraz suplement do dyplomu objaśniający jego zdobyte umiejętności.</p>
            <p>Po ukończeniu nauki technik informatyk może pracować:</p>
            <ul class="list-group mt-5">
                <button type="button" class="list-group-item list-group-item-action">w zakładach pracy wykorzystujących technologie informatyczne zaimplementowane na stanowiskach pracy,</button>
                <button type="button" class="list-group-item list-group-item-action">firmach administrujących sieci komputerowe,</button>
                <button type="button" class="list-group-item list-group-item-action">działach obsługi informatycznej dowolnego przedsiębiorstwa,</button>
                <button type="button" class="list-group-item list-group-item-action">firmach tworzących oprogramowanie komputerowe,</button>
                <button type="button" class="list-group-item list-group-item-action">punktach serwisowych,</button>
                <button type="button" class="list-group-item list-group-item-action">sklepach komputerowych,</button>
                <button type="button" class="list-group-item list-group-item-action">prowadzić własną działalność gospodarczą w zakresie usług informatycznych.</button>
            </ul>
        </div>
        <div class="col-lg-5 col-md-5 col-sm-12 my-5 mx-auto text-justify">
            <div class="list-group">
                <button type="button" class="list-group-item list-group-item-action">Pracować w różnych strukturach sieci komputerowych,</button>

                <button type="button" class="list-group-item list-group-item-action">Projektować i wykonywać małą sieć komputerową opartą o różne technologie,</button>
                <button type="button" class="list-group-item list-group-item-action">Konfigurować urządzenia sieciowe typu router-router,</button>
                <button type="button" class="list-group-item list-group-item-action">Posługiwać się językiem obsługi wybranych rodzajów baz danych, w tym językiem SQL,</button>
                <button type="button" class="list-group-item list-group-item-action">Korzystać z języków programowania!</button>
                <button type="button" class="list-group-item list-group-item-action">Obsługiwać oprogramowanie użytkowe,</button>
                <button type="button" class="list-group-item list-group-item-action">Skonfigurować sprzęt i oprogramowanie,</button>
                <button type="button" class="list-group-item list-group-item-action">Obsługiwać urządzenia peryferyjne (UPS, skanery, drukarki, plotery, kamery cyfrowe).</button>
            </div>
        </div>
    </div>
</div>
  <aside class='row my-5 py-5'> </aside>