<?php

use yii\helpers\Html;
use frontend\assets\AppAsset;
?>
<div class="mx-5">
    <div class="panel panel-default text-center">
        <div class="row ">
            <div class="col-lg-10 col-md-10 col-sm-10 my-5 py-5 mx-auto panel-heading bg-dark text-light">
                <p style="font-size: 26px;">Technikum Informatyczne jest częścią Zespołu Szkół Ponadgimnazjalnych w Iłowej</p>
                 <?= Html::img('@web/img/logo.png', ['alt' => 'ZSPIlowa', 'class' => 'image rounded', 'style' => 'height: 100px;']); ?>
                <p><br/>(kontakt poniżej)</p>
            </div>
        </div>
        <div class="row panel-body">
            <div class="col-lg-6 col-md-6 col-sm-12 my-2 py-2 mx-auto">
                <i class="fas fa-phone-square fa-10x text-dark zsp"></i>
            </div>

            <div class="col-lg-6 col-md-6 col-sm-12 text-justify mx-auto my-2 py-2 text-muted">
                <p>E-mail szkoły: zspilowa@powiatzaganski.pl<br/>
                    E-mail administratora strony: nanquery@gmail.com<br/></p>
                <p>Telefon: tel. (068) 377-43-14<br/>
                    lub (068) 377-43-30</p>
                <p>Adres szkoły: ul. Pałacowa 1,<br/> 68-120 Iłowa</p>
            </div>
        </div>
    </div>
    
         <aside class='row my-5 py-5 px-5'></aside>
         <hr class="mx-5"/>