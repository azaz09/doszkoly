<?php

use frontend\assets\AppAsset;
use common\widgets\Alert;
use common\models\Media;
use common\models\Article;

$article = new Article();
$media = new Media();
AppAsset::register($this);
?>

 <aside class='row my-5 py-5'> </aside>
<div class="panel panel-default py-5 my-5">
    <h2 class='py-2 my-5 pt-5 text-center text-debian lobster my-auto'>Wideo</h2>

    <div class = 'd-flex flex-row flex-wrap my-4'>
        <?php $media->showMedia('video', 'sometimes'); ?>
    </div>
</div>
 <aside class='row my-5 py-5'> </aside>
<div class="panel panel-default py-5 my-5">    

    <h3 class='py-2 my-5 text-center text-info lobster'>Zdjęcia</h3>

    <div class = 'd-flex flex-row flex-wrap my-4'>
        <?php $media->showMedia('image', 'sometimes'); ?>
    </div>
</div>
 <aside class='row my-5 py-5'> </aside>