<?php
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use frontend\assets\AppAsset;
use yii\bootstrap\Dropdown;
use common\models\Article;
use common\models\search\ArticleSearch;
use yii\widgets\ActiveForm;

$model = new Article();
$modelSearch = new ArticleSearch();
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="keywords" content="TechnikInformatyk, technik, informatyk, urządzenia, urzadzenia, komputery, sprzęt, sprzet, kwalifikacje, programowanie, montaż, montaz, eksploatacja, tutorial, pomoc, help, yii2, pasja, informatyki, zdjęcia, zdjecia, filmy, video, youtube, artykuły, artykuly, systemy, operacyjne, linux, debian, ubuntu, kali, fedora, archlinux, przygotowanie, egzaminy, HTML, CSS, c++, JS, PHP, C++, how2do, how2, zspilowa, ilowa, zsp, szkoła, szkola, szkolne, EE.08, EE.09, E.12, E.13, E.14, kompedium, wiedzy, zawód, zawod">
        <meta name="description" content="Strona w przejrzysty sposób przedstawia wiedzę, filmy i zdjęcia z zakresu Informatyki. Pomaga przygotować się do egzaminu EE.08, EE.09 i odkryć nowy materiał z szerokiej gamy.">
        <meta name="author" content="Albert Jurasik">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
        <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet"> 
        <link href="https://fonts.googleapis.com/css?family=Lobster" rel="stylesheet"> 
        
        <link href="../web/js/lightbox2-master/dist/css/lightbox.css" rel="stylesheet">
        <link rel="Shortcut icon" href="http://www.zsp2.edu.pl/oferta/images/informatyk/ikonka.png" />
        
    </head>
    <body>
        <?php $this->beginBody() ?>

        <div class="container wrap mb-0">
            <div class="panel panel-default" style="height: 100%;">
                <?php
                NavBar::begin([
                    'brandLabel' => Yii::$app->name,
                    'brandUrl' => Yii::$app->homeUrl,
                    'options' => [
                        'class' => 'navbar-inverse navbar-fixed-top py-0 mb-0 mx-0 navbar-expand-lg navbar-expand-md',
                    ],
                ]);
                $menuItems = [
                    ['label' => ' Strona główna', 'url' => ['/site/index'],
                        'options' => [
                            'class' => 'text-center',
                        ]
                    ],
                ];
                if (Yii::$app->user->isGuest) {

                    $menuItems[] = ['label' => 'Login', 'url' => Yii::$app->urlManagerB->baseUrl];
                } else {
                    $menuItems[] = '<li>'
                            . Html::beginForm(['/site/logout'], 'post')
                            . Html::submitButton(
                                    'Logout (' . Yii::$app->user->identity->username . ')', ['class' => 'btn btn-link logout']
                            )
                            . Html::endForm()
                            . '</li>';
                }
                echo Nav::widget([
                    'options' => ['class' => 'navbar-nav navbar-right text-align-right'],
                    'items' => $menuItems,
                ]);
                NavBar::end();
                ?>
                <div class="row my-3 mx-auto mb-0 pb-0">

                    <div class="col-lg-12 col-md-12 col-sm-12" style="">
                        <div id="CarouselButton" class="carousel slide" data-ride="carousel" >
                            <ol class="carousel-indicators">
                                <li data-target="#CarouselButton" data-slide-to="0" class="active"></li>
                                <li data-target="#CarouselButton" data-slide-to="1"></li>
                                <li data-target="#CarouselButton" data-slide-to="2"></li>
                                <li data-target="#CarouselButton" data-slide-to="3"></li>
                                <li data-target="#CarouselButton" data-slide-to="4"></li>
                            </ol>
                            <div class="carousel-inner">
                                <div class="carousel-item active">
                                    <?= Html::img('https://lh3.googleusercontent.com/-M01Hm7Rjcfk/WyKHG90jrnI/AAAAAAAAADA/OEt0Eow2Bf08wfrdrqQj7xBXMjEwsX4KACJoC/w530-h406-n-rw/img1.jpg', ['alt' => '1 slajd', 'class' => 'd-block image w-100 rounded']); ?>

                                </div>
                                <div class="carousel-item">
                                    <?= Html::img('https://images83.fotosik.pl/1116/d9ea3c964f1a5e1emed.jpg', ['alt' => '2 slajd', 'class' => 'd-block image w-100 rounded']); ?>

                                </div>
                                <div class="carousel-item">
                                    <?= Html::img('https://images84.fotosik.pl/1117/2fa3fbae190cba35med.jpg', ['alt' => '3 slajd', 'class' => 'd-block image w-100 rounded']); ?>

                                </div>
                                <div class="carousel-item">
                                    <?= Html::img('https://images84.fotosik.pl/1117/cd61c23059c96cf6med.jpg', ['alt' => '4 slajd', 'class' => 'd-block image w-100 rounded']); ?>

                                </div>
                                <div class="carousel-item">
                                    <?= Html::img('https://images82.fotosik.pl/1119/da0821908353fa93med.jpg', ['alt' => '5 slajd', 'class' => 'd-block image w-100 rounded']); ?>

                                </div>
                            </div>
                            <a class="carousel-control-prev " href="#CarouselButton" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                
                            </a>
                            <a class="carousel-control-next" href="#CarouselButton" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                               
                            </a>
                        </div>

                    </div>
                </div>

                <nav class="navbar navbar-expand-lg navbar-expand-md navbar-light bg-light mx-3 mb-5 mt-0 pt-0">

                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation" placeholder="Wyszukaj">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item dropdown">
                                <a href="#" data-toggle="dropdown" class="px-5 dropdown-toggle mx-auto btn" aria-haspopup="true" aria-expanded="false"><i class="far fa-newspaper"></i> Artykuły</a>
                                <?php
                                echo Dropdown::widget([
                                    'items' => [
                                        ['label' => 'Strona główna', 'url' => ['site/index']],
                                        ['label' => 'EE.08', 'url' => ['site/eight']],
                                        ['label' => 'EE.09', 'url' => ['site/nine']],
                                        ['label' => 'Inne', 'url' => ['site/other']],
                                    ],
                                    'options' => ['style' => 'font-size: 16px !important;', 'class' => 'rounded-bottom border border-info text-center'],
                                ]);
                                ?>
                            </li>
                            <li class="nav-item dropdown">
                                <a href="#" data-toggle="dropdown" class="px-5 dropdown-toggle mx-auto btn" aria-haspopup="true" aria-expanded="false"><i class="fab fa-youtube"></i> Media</a>
                                <?php
                                echo Dropdown::widget([
                                    'items' => [
                                        ['label' => 'Kwalifikacje', 'url' => ['site/category']],
                                        ['label' => 'Dzień z życia', 'url' => ['site/sometimes']],
                                        ['label' => 'Inne', 'url' => ['site/other']],
                                    ],
                                    'options' => ['style' => 'font-size: 16px !important;', 'class' => 'rounded-bottom border border-info text-center'],
                                ]);
                                ?>
                            </li>

                            <li class="nav-item dropdown">
                                <a href="#" data-toggle="dropdown" class="px-5 dropdown-toggle mx-auto btn" aria-haspopup="true" aria-expanded="false"><i class="far fa-address-card"></i> O nas</a>
                                <?php
                                echo Dropdown::widget([
                                    'items' => [
                                        ['label' => 'Kontakt', 'url' => ['site/contact']],
                                        ['label' => 'Informacje', 'url' => ['site/technical']],
                                    ],
                                    'options' => ['style' => 'font-size: 16px !important;', 'class' => 'rounded-bottom border border-info text-center'],
                                ]);
                                ?>
                            </li>
                        </ul>


                    </div>

                </nav>
                <aside class='row my-5 py-5'> </aside>
                <!--Download other pages here-->          
                <?= $content ?>
                <!---->
                <aside class='row my-5 py-5'> </aside>
                <div class="footer mt-5 pt-5 border-top rounded">
                    <div class="row text-center mx-auto py-3 bg-light">
                        <div class=" col-lg-4 col-md-4 col-sm-4 border-right">
                            <div class="my-2 px-2 text-muted">
                                <a class="footer-a fb" target="blank" href="https://www.facebook.com/zspilowa/"><i class="fab fa-facebook fa-3x"></i>
                                    <br/>
                                    <br/>
                                    Strona Facebook
                                </a>
                            </div>
                        </div>
                        <div class=" col-lg-4 col-md-4 col-sm-4">
                            <div class="my-2 px-2 text-muted border-right">
                                <a class="footer-a cl" target="blank"  href="http://www.zspilowa.pl/claroline/"><i class="fas fa-feather fa-3x "></i>
                                    <br/>
                                    <br/>
                                    Portal
                                    Claroline
                                </a>
                            </div>
                        </div>
                        <div class=" col-lg-4 col-md-4 col-sm-4">
                            <div class="my-2 px-2 text-muted">
                                <a class="footer-a zsp" target="blank" href="http://zspilowa.edupage.org/"><i class="far fa-eye fa-3x"></i>
                                    <br/>
                                    <br/>
                                    Strona szkoły
                                </a>
                            </div>
                        </div>

                    </div>
                   
                    <div class="row text-center py-5 px-5 pt-5">
                        <div class="col-lg-6 col-md-6 col-sm-6 mx-auto my-auto">
                            <h3 class="text-primary lobster rounded" style="font-weight: 200; border-bottom: 3px dotted #dee2e6;">Galeria:</h3>
                            <a href="https://images83.fotosik.pl/1116/aecc27b8398d616emed.jpg" caption="naprawa uszkodzonych części w dronie" data-lightbox="roadtrip"> <img src='https://images83.fotosik.pl/1116/aecc27b8398d616emed.jpg' class='d-inline-block rounded block-border my-2 mx-auto' /></a>
                             <a href="https://images82.fotosik.pl/1119/802d4dd77b62be4f.jpg" caption="wymiana procesora intel core 2 duo" data-lightbox="roadtrip"> <img src='https://images82.fotosik.pl/1119/802d4dd77b62be4f.jpg' class='d-inline-block rounded block-border mx-auto my-2' /></a>
                              <a href="https://images82.fotosik.pl/1119/7363927ccb16d107med.jpg" caption="praca z grafiką" data-lightbox="roadtrip"> <img src='https://images82.fotosik.pl/1119/7363927ccb16d107med.jpg' class='d-inline-block rounded block-border mx-auto my-2' ></a>
                         <a href="https://images84.fotosik.pl/1117/cd61c23059c96cf6med.jpg" caption="praca ze sprzętem" data-lightbox="roadtrip"> <img src='https://images84.fotosik.pl/1117/cd61c23059c96cf6med.jpg' class='d-inline-block rounded block-border mx-auto my-2' ></a>
                         <a href="https://images84.fotosik.pl/1117/37019d74bb023bb3med.jpg" caption="Zapracowani" data-lightbox="roadtrip"> <img src='https://images84.fotosik.pl/1117/37019d74bb023bb3med.jpg' class='d-inline-block rounded block-border mx-auto my-2' ></a>
                         <a href="https://images82.fotosik.pl/1119/da0821908353fa93med.jpg" caption="Zapracowani" data-lightbox="roadtrip"> <img src='https://images82.fotosik.pl/1119/da0821908353fa93med.jpg' class='d-inline-block rounded block-border mx-auto my-2' ></a>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 mx-auto my-auto" >
                            <i class="fas fa-quote-right fa-3x py-3 pb-2"></i><br/><br/>
                            Nie porównuj siebie do innych,<br/> jedyną osobą, od której powinieneś być lepszy<br/> jesteś ty sam z dnia wczorajszego.
                        </div>
                        
                    </div>

                    <div class="row py-3 text-center">

                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <hr/>
                            Webdev. Albert Jurasik &copy; Prawa do strony zastrzeżone!
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <?php
       
        $this->registerJsFile(
                '@web/js/lightbox2-master/dist/js/lightbox-plus-jquery.js', ['depends' => [\yii\web\JqueryAsset::className()], 'async' => 'async']
        );
        $this->registerJsFile(
                '@web/js/display.js', ['depends' => [\yii\web\JqueryAsset::className()], 'async' => 'async']
        );
        ?>
        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
